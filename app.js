document.addEventListener("DOMContentLoaded", () => {
  let score = 0;
  let nextPiece = 0;
  let highScore = 0;
  let lines = 0;
  let currentLines = 0;
  let rows = [];
  let height = 0;

  const canvas = document.getElementById("board");
  const context = canvas.getContext("2d");

  context.canvas.width = COLS * BLOCK_SIZE;
  context.canvas.height = ROWS * BLOCK_SIZE;

  context.scale(BLOCK_SIZE, BLOCK_SIZE);

  //get 10 points for every piece dropped on board
  const updateScore = () => {
    score = score + 10;
    document.getElementById("score").innerHTML = "Score: " + score;
  };

  const rowClear = () => {
    if (currentLines === 4) {
      score = score * 1000; //4 lines gets you 1000x
    } else if (currentLines === 3) {
      score = score * 500; //3 lines gets you 500x etc...
    } else if (currentLines === 2) {
      score = score * 250;
    } else if (currentLines === 1) {
      score = score * 100;
    }
  };
  //check if row is 'full/ready to clear'
  if (row.length === 10) {
    currentLines = currentLines + 1; /* ISSUE! Can't have more than
      one line at a time */
    rowClear(); //run row clear function
  }

  function controls() {
    alert(
      "Press 'a', or 's' to rotate the piece.\r\nPress the left/right arrow key to move the piece left, or right.\r\nPress the up arrow key to hard-drop, or down arrow key to soft-drop the piece."
    );
  }

  function gameStart() {
    console.log("hello");
  }

  function gameOver() {
    if (score > highScore) {
      highScore = score;
    }
    score = 0;
    lines = 0;
    alert("Game Over!");
  }
});
