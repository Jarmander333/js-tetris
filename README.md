# js-tetris

Javascript Tetris Clone

## Name
JavaScript Tetris Game

## Description
This is our first attempt at making a game using JavaScript. We decided to start with a classic, Tetris. After we finish the base game we will add different variations like 5 lengthed shapes or 3-D Tetris.

## Installation
...

## Controls
TBD

## Roadmap
Work on making the base game.
After we finish the base game we will add different variations like 5 lengthed shapes or 3-D Tetris.


## Authors and acknowledgment
Creators: Jarmander333 and Siembiedak

## License
For open source projects, say how it is licensed.

## Project status
Beginning development

